<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Decimálisból unárisba átváltó Turing gép</title>
        <para>
            Állapotátmenet gráfjával megadva írd meg ezt a gépet!
        </para>
        <para>
            Az unáris számrendszer csak egy fajta karaktert ismer. Ez legyen az 1. Ha egy decimális számot át akarunk váltani unáris számrendszerbe, akkor annyi 1-est kell felírni, amennyi maga a szám értéke.
            Szóval például a 3 az '111'-nek felel meg.
            Itt a példában egy ciklus fut le addig, amennyi maga a szám és kiír annyi 1-est.
            <programlisting language="c"><![CDATA[function atvalto(int n){
    int i;
    for (i = 0; i < n; i++)
    {
        printf("1");
    }
}]]></programlisting>
        </para>
    </section>        
        
    <section>
        <title>Az a<superscript>n</superscript>b<superscript>n</superscript>c<superscript>n</superscript> nyelv nem környezetfüggetlen</title>
        <para>
            Mutass be legalább két környezetfüggő generatív grammatikát, amely ezt a nyelvet generálja!
        </para>
        <para>
            SKIP
        </para>
    </section>        
                
    <section>
        <title>Hivatkozási nyelv</title>
        <para>
            A <citation>KERNIGHANRITCHIE</citation> könyv C referencia-kézikönyv/Utasítások melléklete alapján definiáld 
            BNF-ben a C utasítás fogalmát!
            Majd mutass be olyan kódcsipeteket, amelyek adott szabvánnyal nem fordulnak (például C89), mással (például C99) igen.
        </para>
        <para>
            A c89 szabvány ANSI kódolást alkalmaz, ezért nem minden esetben támogatja a szöveg lefordítását.
            C89-ben egy for ciklus így néz ki:
            <programlisting language="c"><![CDATA[int i;
for (i = 0; i < 100; i++)
{
   doSomething();
}]]></programlisting>
            A c99 alapértelmezett szabvány a C programnyelvnek.
            C99-ben egy for ciklus így néz ki:
            <programlisting language="c"><![CDATA[for (int i = 0; i < 100; i++)
{
   doSomething();
}]]></programlisting>
        </para>
    </section>                     

    <section>
        <title>Saját lexikális elemző</title>
        <para>
            Írj olyan programot, ami számolja a bemenetén megjelenő valós számokat! 
            Nem elfogadható olyan megoldás, amely maga olvassa betűnként a bemenetet, 
            a feladat lényege, hogy lexert használjunk, azaz óriások vállán álljunk és ne kispályázzunk!
        </para>
        <para>
            A regex megszámolja a számokat, mivel csak digit (szám) karaktereket érzékel.
            <programlisting language="c"><![CDATA[%{
#include <stdio.h>
int realnumbers = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{++realnumbers; 
    printf("[realnum=%s %f]", yytext, atof(yytext));}
%%
int
main ()
{
 yylex ();
 printf("The number of real numbers is %d\n", realnumbers);
 return 0;
}
]]></programlisting>

        </para>
    </section>                     

    <section>
        <title>l33t.l</title>
        <para>
            Lexelj össze egy l33t ciphert!
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/Mark018/prog1/blob/master/Cipher/l33t.l">https://gitlab.com/Mark018/prog1/blob/master/Cipher/l33t.l</link>
        </para>
        <para>
            Definiálunk egy számot vissza adó funkciót, ami visszatér a l337d1c7 tábla méretével.
            <programlisting language="c">#define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher))</programlisting>
            Ezt követően elkészítjük a kódoláshoz szükséges táblát.
            Ez a tábla tartalmazza a karaktereket, amiket kicserélünk egy másikra.
            Jelen esetben ez 4 új karaktert jelent, amik véletlenszerűen vannak kiválasztva, kisebb-nagyobb esélyel.
            A legtöbb esély (90%) a 4. esetre utal. Fontos, hogy egy betűhöz ne tartozzon ugyan az a karakter sorozat, mert különben nem lehet visszafejteni.
            <programlisting language="c"><![CDATA[  struct cipher {
    char c;
    char *leet[4];
  } l337d1c7 [] = {

  {'a', {"4", "4", "@", "/-\\"}},
  {'b', {"b", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };]]></programlisting>

        A program alapja egy for ciklus. Ez felelős azért, hogy a karakterek ki legyenek cserélve.
        A ciklusban az if megvizsgálja a karaktert, ha egyezik akkor kiírja random esélyel az egyik megfelelő kódolt alakját.
        Ha nem egyezik egyikkel sem, akkor vissza írja magát a karaktert.
        </para>
    </section>

    <section>
        <title>A források olvasása</title>
        <para>
            Hogyan olvasod, hogyan értelmezed természetes nyelven az alábbi kódcsipeteket? Például
            <programlisting><![CDATA[if(signal(SIGINT, jelkezelo)==SIG_IGN)
    signal(SIGINT, SIG_IGN);]]></programlisting>
            Ha a SIGINT jel kezelése figyelmen kívül volt hagyva, akkor ezen túl is legyen
            figyelmen kívül hagyva, ha nem volt figyelmen kívül hagyva, akkor a jelkezelo függvény
            kezelje. (Miután a <command>man 7 signal</command> lapon megismertem a SIGINT jelet, a
            <command>man 2 signal</command> lapon pedig a használt rendszerhívást.)
        </para>

        <caution>
            <title>Bugok</title>
            <para>
                Vigyázz, sok csipet kerülendő, mert bugokat visz a kódba! Melyek ezek és miért? 
                Ha nem megy ránézésre, elkapja valamelyiket esetleg a splint vagy a frama?
            </para>
        </caution>
            
        <orderedlist numeration="lowerroman">
            <listitem>                                    
                <programlisting><![CDATA[if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
    signal(SIGINT, jelkezelo);]]></programlisting>
            </listitem>
            <listitem>                                    
                <programlisting><![CDATA[for(i=0; i<5; ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<5; i++)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<5; tomb[i] = i++)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[for(i=0; i<n && (*d++ = *s++); ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(a, ++a), f(++a, a));]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(a), a);]]></programlisting>            
            </listitem>
            <listitem>                                    

                <programlisting><![CDATA[printf("%d %d", f(&a), a);]]></programlisting>            
            </listitem>
        </orderedlist>

        <para>
            Ha nincsen ignorálva a kapott signal, akkor meghívja a jelkezelo funkciót.
        </para>

    </section>                     

    <section>
        <title>Logikus</title>
        <para>
            Hogyan olvasod természetes nyelven az alábbi Ar nyelvű formulákat?
        </para>
        <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})))$ 

$(\forall x \exists y ((x<y)\wedge(y \text{ prím})\wedge(SSy \text{ prím})))$ 

$(\exists y \forall x (x \text{ prím}) \supset (x<y)) $ 

$(\exists y \forall x (y<x) \supset \neg (x \text{ prím}))$
]]></programlisting>        
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/MatLog_LaTeX</link>
        </para>

        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/ZexiPy3ZxsA">https://youtu.be/ZexiPy3ZxsA</link>, <link xlink:href="https://youtu.be/AJSXOQFF_wk">https://youtu.be/AJSXOQFF_wk</link>
        </para>

        <para>
            SKIP
        </para>
    </section>

    <section>
        <title>Deklaráció</title>
            
        <para>
            Vezesd be egy programba (forduljon le) a következőket: 
        </para>

        <itemizedlist>
            <listitem>
                <para>egész</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egész referenciája</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészek tömbjének referenciája (nem az első elemé)</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatók tömbje</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvény</para>                        
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvényre mutató mutató</para>                        
            </listitem>
            <listitem>
                <para>egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény</para>                        
            </listitem>            
            <listitem>
                <para>függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre</para>                        
            </listitem>            
        </itemizedlist>            

       <para>
            Mit vezetnek be a programba a következő nevek?
        </para>

        <itemizedlist>
            <listitem>
                <programlisting><![CDATA[int a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *b = &a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int &r = a;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int c[5];]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (&tr)[5] = c;]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *d[5];]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *h ();]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int *(*l) ();]]></programlisting>            
            </listitem>
            <listitem>
                <programlisting><![CDATA[int (*v (int c)) (int a, int b)]]></programlisting>            
            </listitem>            
            <listitem>
                <programlisting><![CDATA[int (*(*z) (int)) (int, int);]]></programlisting>            
            </listitem>            
        </itemizedlist>       

        <para>
            Ha mutatóként használunk egy változót, akkor az új változó szerkesztésekor frissül az eredeti változó is.
            Ugyan ez igaz a táblákra és függvényekre is.
            <programlisting language="c"><![CDATA[int main(){
    int a;
    int *b = &a;
    int &r = a;
    int c[5];
    int (&tr)[5] = c;
    int *d[5];
    int *h ();
    int *(*l) ();
    int (*v (int c)) (int a, int b);
    int (*(*z) (int)) (int, int);
    return 0;
}
]]></programlisting>
            Lehet referenciaként is használni a változókat, függvényeket. Ilyenkor egy másik változót referáljuk ugyan erre a változóra, így változtathatjuk vagy lekérhetjük annak értékét.
        </para>
    </section>                     

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
