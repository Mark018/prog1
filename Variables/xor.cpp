#include "../std_lib_facilities.h"

int main()
{
	int a = 40 ^ 15;  // a = 39 | megadható még 40 xor 15
    cout << "Kizárjuk 9-ből a 3-at: " << a << endl;

    int b = a ^ 30; // b = 57; | megadható még a xor 30
    cout << "Kizárjuk " << a << "-ból/ből a 30-at: " << b << endl;
}