#include "../std_lib_facilities.h"

int main()
{
	int a = 40;
    int b = 25;
    int c = 15;

    cout << "Kiindulás:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;
    cout << "C: " << c << endl;

    a = a * b; // a = 1000;
    c = a; // c = 1000;
    b = a / c; // b = 1;

    cout << "Művelet után:" << endl;
    cout << "A: " << a << endl;
    cout << "B: " << b << endl;
    cout << "C: " << c << endl;
}